<?php 
require 'run.php';
require 'fun_koneksi.php';

$game = get_game($_COOKIE['kode']);

if (count($game) == 0) {
	echo json_encode(['status' => 'error', 'message' => 'game code not valid']);
	exit();
}

$data =[
	'now' => $game['now'],
	'game' => $game['game_now'],
	'box' => $game['box_count']
];

echo json_encode($data);
require 'close.php';
 ?>