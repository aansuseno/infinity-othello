<?php
require 'fun_koneksi.php';
require 'fun_legal.php';

function jml()
{
	global $game;
	$p = json_decode($game['game_now']);
	$total = [0,0];

	foreach ($p as $v) {
		foreach ($v as $d) {
			if ($d == 'b') {
				$total[0]++;
			} else if ($d == 'w') {
				$total[1]++;
			}
		}
	}

	return $total;
}

if ($game['is_end']) {
	$diff = abs(strtotime($game['created_at']) - strtotime($game['updated_at']));
} else {
	$diff = abs(strtotime($game['created_at']) - strtotime(date('Y-m-d H:i:s')));
}

$selisih = "";

if ($diff > (60*60*24*7*30) && $diff > 0) {
	$sel = floor($diff/(60*60*24*7*30));
	$diff = $diff - ($sel *(60*60*24*7*30));
	$selisih .= " ".$sel." month".(($sel > 1) ? 's':'');
}

if ($diff > (60*60*24*7) && $diff > 0) {
	$sel = floor($diff/(60*60*24*7));
	$diff = $diff - ($sel *(60*60*24*7));
	$selisih .= " ".$sel." week".(($sel > 1) ? 's':'');
}

if ($diff > (60*60*24) && $diff > 0) {
	$sel = floor($diff/(60*60*24));
	$diff = $diff - ($sel *(60*60*24));
	$selisih .= " ".$sel." day".(($sel > 1) ? 's':'');
}

if ($diff > (60*60) && $diff > 0) {
	$sel = floor($diff/(60*60));
	$diff = $diff - ($sel *(60*60));
	$selisih .= " ".$sel." hour".(($sel > 1) ? 's':'');
}

if ($diff > 60 && $diff > 0) {
	$sel = floor($diff/60);
	$diff = $diff - ($sel *(60));
	$selisih .= " ".$sel." minute".(($sel > 1) ? 's':'');
}

if ($diff > 0) {
	$selisih .= " ".$diff." second".(($sel > 1) ? 's':'');
}

echo json_encode([
	'jml' => json_encode(jml()),
	'selisih' => $selisih,
	'end' => $game['is_end'],
	'last_action' => $game['updated_at'],
	'infinity' => $game['is_infinity'],
	'diff' => $diff
]);
?>