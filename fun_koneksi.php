<?php 

$db_name = 'db.db';
$db = new SQLite3($db_name);
$db->busyTimeout(1000);

function get_game($kode = '-')
{
	global $db;
	$kode = ($kode=='-') ? $_COOKIE['kode'] : $kode;
	return $db->querySingle("SELECT * FROM game where kode = '$kode'", true);
}

function in_alur($id_game, $move, $color, $change, $position, $id, $game_ke, $box_count)
{
	global $db;
	$date = date('Y-m-d H:i:s');
	$color_r = ($color == 'b') ? 'w' : 'b';
	$game_ke += 1;
	$db->exec("INSERT into alur (id_game, move, color, created_at, updated_at, game_ke) values ($id_game, '$move', '$color', '$date', '$date', $game_ke)");
	$db->exec("UPDATE game SET updated_at='$date', now='$color_r', change='$change', game_now='$position', game_ke=$game_ke, box_count=$box_count WHERE id=$id");
}

function join_game($id)
{
	global $db;
	$date = date('Y-m-d H:i:s');
	$db->exec("UPDATE game SET updated_at='$date', still_open=1 WHERE id=$id");
}

function skip($id_game, $color, $game_ke)
{
	global $db;
	$date = date('Y-m-d H:i:s');
	$color_r = ($color == 'b') ? 'w' : 'b';
	$game_ke += 1; 
	$db->exec("UPDATE game SET updated_at='$date', now='$color_r', game_ke=$game_ke WHERE id=$id_game");
}

function get_before_move($id_game, $game_ke)
{
	global $db;
	$game_ke -= 1; 
	return $db->querySingle("SELECT * FROM alur where id_game = '$id_game' and game_ke=$game_ke", true);
}

function end_game($id_game)
{
	global $db;
	$date = date('Y-m-d H:i:s');
	$db->exec("UPDATE game SET updated_at='$date', is_end=1 WHERE id=$id_game");
}

function change_infinity_status($now, $id)
{
	global $db;
	$date = date('Y-m-d H:i:s');
	$db->exec("UPDATE game SET updated_at='$date', is_infinity=$now WHERE id=$id");
}
?>