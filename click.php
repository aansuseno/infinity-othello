<?php 
require 'run.php';
require 'fun_koneksi.php';
require 'fun_legal.php';


function join_arr($data)
{
	return $data[0]."-".$data[1];
}

$legal = find_legal_move();
$coordinate = join_arr($_POST['coordinate']);
$legal_j = array_map('join_arr', $legal);
$i = (int)$_POST['coordinate'][0];
$j = (int)$_POST['coordinate'][1];


$token = ($game['now'] == 'b') ? $game['user_token_b'] : $game['user_token_w'];
if (!in_array($coordinate, $legal_j) || $token != $_COOKIE['user_token']) {
	echo json_encode(['status' => 'illegal']);
	exit();
}

function cek_aroud($v,$h) {
	global $position;
	global $now;
	global $box;
	$around = [];
	$b_w = ($now == 'b') ? 'w' : 'b';

	// to top
	if ($v > 0) {
		if ($position[$v-1][$h] == $b_w) {
			array_push($around, [$v-1, $h]);
		}
	}

	// to bottom
	if ($v < $box-1) {
		if ($position[$v+1][$h] == $b_w) {
			array_push($around, [$v+1, $h]);
		}
	}

	// to left
	if ($h > 0) {
		if ($position[$v][$h-1] == $b_w) {
			array_push($around, [$v, $h-1]);
		}
	}

	// to right
	if ($h < $box-1) {
		if ($position[$v][$h+1] == $b_w) {
			array_push($around, [$v, $h+1]);
		}
	}

	// to top left
	if($v > 0 && $h > 0) {
		if ($position[$v-1][$h-1] == $b_w) {
			array_push($around, [$v-1, $h-1]);
		}
	}

	// to top right
	if($v > 0 && $h < $box-1) {
		if ($position[$v-1][$h+1] == $b_w) {
			array_push($around, [$v-1, $h+1]);
		}
	}

	// to bottom left
	if($v < $box-1 && $h > 0) {
		if ($position[$v+1][$h-1] == $b_w) {
			array_push($around, [$v+1, $h-1]);
		}
	}

	// to bottom right
	if($v < $box-1 && $h < $box-1) {
		if ($position[$v+1][$h+1] == $b_w) {
			array_push($around, [$v+1, $h+1]);
		}
	}

	return $around;
}

function follow_with_data($from, $data, $b_w, $b_w_n, $arr = []) {
	global $box;
	global $position;
	$now = [$from[0]-$data[0], $from[1]-$data[1]];

	// cek show can cross border
	if ($now[0] < 0 || $now[1] < 0 || $now[0] >= $box || $now[1] >= $box) {
		$arr = [];
		return $arr;
	}

	$b_w_i = $position[$now[0]][$now[1]];
	if ($b_w_i == $b_w) {
		array_push($arr, $now);
		return follow_with_data($now, $data, $b_w, $b_w_n, $arr);
	} else if ($b_w_i == $b_w_n) {
		return $arr;
	} else {
		$arr = [];
		return $arr;
	}
}

function change_all()
{
	global $now;
	global $i;
	global $j;
	$change = [];
	$allaround = cek_aroud($i, $j);
	foreach ($allaround as $val) {
		$diff = [$i-$val[0],$j-$val[1]];
		$b_w = ($now == 'b') ? 'w' : 'b';
		array_push($change, follow_with_data([$i,$j], $diff, $b_w, $now));
	}

	array_push($change, [[$i, $j]]);

	return $change;
}

function exec_game($change)
{
	global $position;
	global $now;
	global $i;
	global $j;
	foreach ($change as $val) {
		foreach ($val as $v) {
			$position[$v[0]][$v[1]] = $now;
		}
	}
}

$change = change_all();
exec_game($change);

// jika ada yg di pinggir
$posisi_baru = [];
if (($i == 0 || $j == 0 || $i == $game['box_count']-1 || $j == $game['box_count']-1) && $game['is_infinity'] == 1) {
	$jml = $game['box_count'];
	for ($k=0; $k < $jml+4; $k++) { 
		array_push($posisi_baru, []);
		for ($x=0; $x < $jml+4; $x++) { 
			$fill_it = '-';
			if ($k >= 2 && $x >= 2 && $k < $jml+2 && $x < $jml+2) {
				$fill_it = $position[$k-2][$x-2];
			}
			array_push($posisi_baru[$k], $fill_it);
		}
	}

	// change the 'change'
	foreach ($change as $key => $value) {
		foreach ($value as $k => $v) {
			$change[$key][$k] = [$v[0]+2, $v[1]+2];
		}
	}
}

if (count($posisi_baru) > 0) {
	in_alur($game['id'], $coordinate, $game['now'], json_encode($change), json_encode($posisi_baru), $game['id'], $game['game_ke'], $game['box_count']+4);
} else {
	in_alur($game['id'], $coordinate, $game['now'], json_encode($change), json_encode($position), $game['id'], $game['game_ke'], $game['box_count']);
}
echo json_encode([
	'status' => 'oke',
	'change' => json_encode($change),
	'color' => $now
]);
?>