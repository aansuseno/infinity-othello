<?php 
require 'run.php';
require 'fun_koneksi.php';
require 'fun_legal.php';


if ($user_b || $user_w) {
	echo json_encode([
		'you' => 'yes',
		'legal' => json_encode(find_legal_move()),
		'change' => $game['change'],
		'color' => ($game['now'] == 'b') ? 'w' : 'b',
		'end' => $game['is_end'],
		'box' => $game['box_count'],
	]);
} else {
	echo json_encode([
		'you' => 'no',
		'end' => $game['is_end'],
		'box' => $game['box_count']]);
}

$db->close();
unset($db);

?>