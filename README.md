# INFINITY OTHELLO

This is the infinity version of Othello, besides playing unlimited you can also play it like the normal standard Othello. This game is a game made by me that responds directly, you could say the first 'online' game made by me.

To play it you only need a PHP server that has Sqlite3 installed. Easy as that.

Check the preview video [here](https://drive.google.com/file/d/17lTs0s-qfgoIyvp_8PjNenPl8Jx8D4my/view?usp=sharing).
