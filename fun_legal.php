<?php
$game = get_game($_COOKIE['kode']);

if (count($game) == 0) {
	echo json_encode(['status' => 'error', 'message' => 'game code not valid']);
	exit();
}

$now = $game['now'];
$box = $game['box_count'];
$user_b = $_COOKIE['user_token'] == $game['user_token_b'] && $now == 'b';
$user_w = $_COOKIE['user_token'] == $game['user_token_w'] && $now == 'w';
$position = json_decode($game['game_now']);

function find_legal_move() {
	global $game;
	global $position;
	global $now;
	$have_room = [];
	$legal_now = [];
	$b_w = ($now == 'b') ? 'w' : 'b';

	for ($i = 0; $i < $game['box_count']; $i++) {
		for ($j = 0; $j < $game['box_count']; $j++) {
			if ($position[$i][$j] == $b_w) {
				$temp = around_have_room($i,$j);
				if (count($temp) > 0) {
					array_push($have_room, ['ori' => [$i, $j], 'pos' => $temp]);
				}
			}
		}
	}

	if (count($have_room) > 0) {
		foreach ($have_room as $i) {
			foreach ($i['pos'] as $j) {
				$selisih = [$i['ori'][0]-$j[0], $i['ori'][1]-$j[1]];
				if(follow_me($j, $selisih, $b_w, $now)) {
					array_push($legal_now, [$j[0], $j[1]]);
				}
			}
		}
	}

	return $legal_now;
}

function around_have_room($v,$h) {
	global $position;
	global $game;
	global $now;
	$box = $game['box_count'];
	$around = [];
	$b_w = ($now == 'b') ? 'w' : 'b';

	// to top
	if ($v > 0) {
		if ($position[$v-1][$h] == '-') {
			array_push($around, [$v-1, $h]);
		}
	}

	// to bottom
	if ($v < $box-1) {
		if ($position[$v+1][$h] == '-') {
			array_push($around, [$v+1, $h]);
		}
	}

	// to left
	if ($h > 0) {
		if ($position[$v][$h-1] == '-') {
			array_push($around, [$v, $h-1]);
		}
	}

	// to right
	if ($h < $box-1) {
		if ($position[$v][$h+1] == '-') {
			array_push($around, [$v, $h+1]);
		}
	}

	// to top left
	if($v > 0 && $h > 0) {
		if ($position[$v-1][$h-1] == '-') {
			array_push($around, [$v-1, $h-1]);
		}
	}

	// to top right
	if($v > 0 && $h < $box-1) {
		if ($position[$v-1][$h+1] == '-') {
			array_push($around, [$v-1, $h+1]);
		}
	}

	// to bottom left
	if($v < $box-1 && $h > 0) {
		if ($position[$v+1][$h-1] == '-') {
			array_push($around, [$v+1, $h-1]);
		}
	}

	// to bottom right
	if($v < $box-1 && $h < $box-1) {
		if ($position[$v+1][$h+1] == '-') {
			array_push($around, [$v+1, $h+1]);
		}
	}

	// console.log(around)

	return $around;
}

function follow_me($from, $data, $b_w, $b_w_n) {
	global $position;
	global $box;
	$now = [$from[0]+$data[0], $from[1]+$data[1]];

	// cek show can cross border
	if ($now[0] >= 0 && $now[1] >= 0 && $now[0] < $box && $now[1] < $box) {
		if (!($position[$now[0]][$now[1]])) return false;
		
		$b_w_i = $position[$now[0]][$now[1]];
		if ($b_w_i == $b_w) {
			return follow_me($now, $data, $b_w, $b_w_n);
		} else if ($b_w_i == $b_w_n) {
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}
?>
