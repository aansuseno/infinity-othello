var box = 0
var l = 100/box
var now = 'b'
var position
var started = false
const a = alertify
alertify.defaults.glossary.title = 'aan_ say:'
var legal_now = []

$(document).ready(() => {

	// open game
	start_game()
})

const start_game = () => {
	$.getJSON( "start-game.php", function( data ) {
		now = data['now']
		box = data['box']
		position = JSON.parse(data['game'])
	}).done(() => {
		l = 100/box
		set_game()
	})
}

const clicked = (i, j) => {
	var change = []
	var legal_str = legal_now.map((val) => {
		return val[0]+'-'+val[1]
	})

	if (!legal_str.includes(i+'-'+j)) {
		show_info()
		return
	} else {
		$.post("click.php",{coordinate: [i,j]}, (data, status) => {
			var data = JSON.parse(data)
			legal_now = []
			if (data.status == 'oke') {
				show_legal_move()
				JSON.parse(data.change).forEach(i => {
					i.forEach(j => {
						position[j[0]][j[1]] = data.color
						flip([j[0],j[1]], data.color)
					})
				})
				cek_now()
			} // endif

		}); // endpost
	}
}

const set_game = () => {
	create_box()
	position.forEach((i, idx) => {
		i.forEach((j, jdx) => {
			if (j != '-') {
				flip([idx,jdx], j)
			}
		})
	})

	cek_now()
}

const create_box = () => {
	var fill = ''
	var x = 0

	for (var i = 0; i < box; i++) {
		for (var j = 0; j < box; j++) {
			fill+= `<div class="tinybox" id="tb-${i}-${j}" style="
				top: ${i*l}vmin;
				left: ${j*l}vmin;
				height: ${l}vmin;
				width: ${l}vmin;
			" data-tb="${x}" onclick="clicked(${i}, ${j})"></div>`
			x++
		}
	}

	$('#game').html(fill)
}

const flip = (id, color) => {
	$('#tb-'+id[0]+"-"+id[1]).html(`<div class="ball ${color}"></div>`)
}

const cek_now = () => {
	$.getJSON( "cek-now.php", function(res) {
		// cek is game ended
		if (res.end == 1) {
			a.alert('The End...')
			return
		}

		if (res.box != box && box != 0) {start_game()}

		if (res['you'] == 'yes') {
			legal_now = JSON.parse(res.legal)
			if (legal_now.length == 0) {
				a
					.alert(`You have no legal move. The <b>"move"</b> will be given to the opponent.`)
					.set({
						onclose:skip
					})
			}
			show_legal_move()

			// tampilkan perubahan
			JSON.parse(res.change).forEach(i => {
				i.forEach(j => {
					flip([j[0], j[1]], res.color)
				})
			})
		} else {
			setTimeout(cek_now, 1000)
		}
	})
}

const show_legal_move = () => {
	$('.tinybox').removeClass('legal_now')
	legal_now.forEach(i => {
		$('#tb-'+i[0]+'-'+i[1]).addClass('legal_now')
	})
}

const skip = () => {

	$.get("skip.php", function( data ) {
		if (data == 'aman') {
			cek_now()
		} else {
			alert('error')
		}
	});
}

const show_info = () => {

	$.getJSON("detail.php", function(data) {
		var jml = JSON.parse(data.jml)
		var show = `
			That is illegal move.
			<br>
			<table style="width: 100%">
				<tr>
					<td>Zoom</td>
					<td>:
						<button onclick="zoom('out')" class="btn btn-outline-secondary"> - </button>
						<b id="ukuran">${Math. round(l)}</b>
						<button onclick="zoom('in')" class="btn btn-outline-secondary"> + </button>
						<button onclick="zoom('reset')" class="btn btn-outline-secondary"> reset </button>
					</td>
				</tr>
				<tr>
					<td>Infinity</td>
					<td>:
						<button onclick="infinity()" class="btn btn-outline-primary" id="is_infinity">${(data.infinity) ? 'YES' : 'NO'}</button>
					</td>
				</tr>
				<tr>
					<td>Black</td>
					<td>: <b>${jml[0]}</b></td>
				</tr>
				<tr>
					<td>White</td>
					<td>: <b>${jml[1]}</b></td>
				</tr>
				<tr>
					<td>time</td>
					<td>: ${data.selisih}</td>
				</tr>
		`

		if (data.end != 0) {
			show+=`<tr><td>ended</td><td>: ${data.last_action}</td></tr>`
		}

		show+='</table>'
		a.alert(show)
	})

}

const zoom = (i_o) => {
	if (i_o == 'in') {
		l++
	} else if (i_o == 'out') {
		l--
	} else {
		l = 100/box
	}
	$('#ukuran').text(Math.round(l))
	for (var i = 0; i < box; i++) {
		for (var j = 0; j < box; j++) {
			$(`#tb-${i}-${j}`).css({
				top: `${i*l}vmin`,
				left: `${j*l}vmin`,
				height: `${l}vmin`,
				width: `${l}vmin`
			})
		}
	}
}

const infinity = () => {
	$.getJSON('change-infinity.php', (res) => {
		$('#is_infinity').text((res.status) ? 'YES' : 'NO')
	})
}