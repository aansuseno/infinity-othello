<!DOCTYPE html>
<html>
<head>
	<title>Infinity Othello</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
	<style type="text/css">
		* {
			font-family: monospace;
		}

		body {
			background: #AA8B56;
		}
	</style>
	<!-- alertifyjs -->
	<!-- JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.13.1/alertify.min.js" integrity="sha512-JnjG+Wt53GspUQXQhc+c4j8SBERsgJAoHeehagKHlxQN+MtCCmFDghX9/AcbkkNRZptyZU4zC8utK59M5L45Iw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<!-- CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.13.1/css/alertify.min.css" integrity="sha512-IXuoq1aFd2wXs4NqGskwX2Vb+I8UJ+tGJEu/Dc0zwLNKeQ7CW3Sr6v0yU3z5OQWe3eScVIkER4J9L7byrgR/fA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.13.1/css/themes/default.min.css" integrity="sha512-RgUjDpwjEDzAb7nkShizCCJ+QTSLIiJO1ldtuxzs0UIBRH4QpOjUU9w47AF9ZlviqV/dOFGWF6o7l3lttEFb6g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body class="container">
	<div class="card text-center position-absolute top-50 start-50 translate-middle">
		<div class="card-header">
			INFINITY OTHELLO
		</div>
		<div class="card-body">
			<?php if (isset($_COOKIE['kode'])): ?>
				<a href="#" onclick="create_cek()" class="btn btn-primary btn-lg">Create Room</a><br><br>
				<a href="game.php" class="btn btn-primary btn-lg">Go To Last Game</a><br><br>
			<?php else: ?>
				<a href="create-room.php" class="btn btn-primary btn-lg">Create Room</a><br><br>
			<?php endif ?>
		<!-- <a href="#" class="btn btn-primary btn-large">Join Room!!</a> -->
		</div>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>

	<!-- cookie -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/3.0.1/js.cookie.min.js" integrity="sha512-wT7uPE7tOP6w4o28u1DN775jYjHQApdBnib5Pho4RB0Pgd9y7eSkAV1BTqQydupYDB9GBhTcQQzyNMPMV3cAew==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script type="text/javascript">
		// Cookies.set('name', 'aan', { expires: 365})	
		function create_cek() {
			alertify.confirm("You still have game. Continue?",
				function(){
					window.location.href = "create-room.php";
				}
			);
		}
	</script>
</body>
</html>