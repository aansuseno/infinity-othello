var box = 8
var l = 100/box
var now = 'b'
var disabled = []
var position = []
var started = false
const a = alertify
var legal_now = []


$('document').ready(() => {
	create_box()

	// for start game
	clicked('tb-3-4', true)
	clicked('tb-3-3', true)
	clicked('tb-4-3', true)
	clicked('tb-4-4', true)

	setTimeout(() => {
		started = true
		a.alert('note...', 'Black to move')
	}, 1000)

	find_legal_move().then(() => {
		show_legal_move()
	})
	$('.tinybox').on("click", function (event) {
		// var loc = event.target.id.split("-")
		// var v = parseInt(loc[1])
		// var h = parseInt(loc[2])
		// is_legal_move(v,h)
		clicked(event.target.id)
	});
})

function create_box() {
	var fill = ''
	var x = 0

	for (var i = 0; i < box; i++) {
		position.push([])
		for (var j = 0; j < box; j++) {
			fill+= `<div class="tinybox" id="tb-${i}-${j}" style="
				top: ${i*l}vmin;
				left: ${j*l}vmin;
				height: ${l}vmin;
				width: ${l}vmin;
			" data-tb="${x}"></div>`
			position[i][j] = '-'
			x++
		}
	}

	$('#game').html(fill)
}

function clicked(id) {
	var location = id.split("-")
	var change = []

	if (disabled.includes(`${location[1]}-${location[2]}`)) {
		a.alert('warning..', 'that is illegal move')
		return
	}

	if (started) {
		// check place is legal
		if (!legal_now.includes(`${location[1]}-${location[2]}`)) {
			a.alert('warning..', 'that is illegal move')
			return
		}

		var allaround = cek_aroud(parseInt(location[1]),parseInt(location[2]))
		// console.log(allaround)
		allaround.forEach(i => {
			var diff = [parseInt(location[1])-i[0],parseInt(location[2])-i[1]]
			var b_w = (now == 'b') ? 'w' : 'b'
			follow_with_data([parseInt(location[1]),parseInt(location[2])], diff, b_w, now).forEach(j => {
				change.push(j)
			})
		})

		change.forEach(i => {
			flip(i, now)
		})
	}
	
	disabled.push(`${location[1]}-${location[2]}`)
	flip([location[1], location[2]], now)

	$('.tinybox').removeClass('legal_now')
	now = (now == 'b') ? 'w' : 'b'
	find_legal_move().then(() => {
		show_legal_move()
	})
}

function flip(id, color) {
	$('#tb-'+id[0]+"-"+id[1]).html(`<div class="ball" style="
		height: ${l*(80/100)}vmin;
		width: ${l*(80/100)}vmin;
		background: ${(color == 'w') ? 'white' : 'black'};
	"></div>`)
	
	position[id[0]][id[1]] = color
}

// function is_legal_move(v,h) {
// 	if (!started) {return true}
// 	cek_aroud(v,h).then((val) => {
// 		console.log(val)
// 	})
// }

function cek_aroud(v,h) {
	var around = []
	var b_w = (now == 'b') ? 'w' : 'b'

	// to top
	if (v > 0) {
		if (position[v-1][h] == b_w) {
			around.push([v-1, h])
		}
	}

	// to bottom
	if (v < box-1) {
		if (position[v+1][h] == b_w) {
			around.push([v+1, h])
		}
	}

	// to left
	if (h > 0) {
		if (position[v][h-1] == b_w) {
			around.push([v, h-1])
		}
	}

	// to right
	if (h < box-1) {
		if (position[v][h+1] == b_w) {
			around.push([v, h+1])
		}
	}

	// to top left
	if(v > 0 && h > 0) {
		if (position[v-1][h-1] == b_w) {
			around.push([v-1, h-1])
		}
	}

	// to top right
	if(v > 0 && h < box-1) {
		if (position[v-1][h+1] == b_w) {
			around.push([v-1, h+1])
		}
	}

	// to bottom left
	if(v < box-1 && h > 0) {
		if (position[v+1][h-1] == b_w) {
			around.push([v+1, h-1])
		}
	}

	// to bottom right
	if(v < box-1 && h < box-1) {
		if (position[v+1][h+1] == b_w) {
			around.push([v+1, h+1])
		}
	}

	return around
}

async function find_legal_move() {
	var have_room = []
	legal_now = []
	var b_w = (now == 'b') ? 'w' : 'b'
	// var b_w_n = now

	for (var i = 0; i < box; i++) {
		for (var j = 0; j < box; j++) {
			if (position[i][j] == b_w) {
				var temp = around_have_room(i,j)
				if (temp.length > 0) {
					have_room.push({
						ori: [i,j],
						pos: temp
					})
				}
			}
		}
	}

	if (have_room.length > 0) {
		have_room.forEach(i => {
			i['pos'].forEach(j => {
				var selisih = [i['ori'][0]-j[0], i['ori'][1]-j[1]]
				if(follow_me(j, selisih, b_w, now)) {
					legal_now.push(j[0]+'-'+j[1])
				}
			})
		})
	}
}

function show_legal_move() {
	legal_now.forEach(i => {
		var l = i.split('-')
		$('#tb-'+i[0]+'-'+i[2]).toggleClass('legal_now')
		console.log(i)
	})
}

function around_have_room(v,h) {
	var around = []
	var b_w = (now == 'b') ? 'w' : 'b'

	// to top
	if (v > 0) {
		if (position[v-1][h] == '-') {
			around.push([v-1, h])
		}
	}

	// to bottom
	if (v < box-1) {
		if (position[v+1][h] == '-') {
			around.push([v+1, h])
		}
	}

	// to left
	if (h > 0) {
		if (position[v][h-1] == '-') {
			around.push([v, h-1])
		}
	}

	// to right
	if (h < box-1) {
		if (position[v][h+1] == '-') {
			around.push([v, h+1])
		}
	}

	// to top left
	if(v > 0 && h > 0) {
		if (position[v-1][h-1] == '-') {
			around.push([v-1, h-1])
		}
	}

	// to top right
	if(v > 0 && h < box-1) {
		if (position[v-1][h+1] == '-') {
			around.push([v-1, h+1])
		}
	}

	// to bottom left
	if(v < box-1 && h > 0) {
		if (position[v+1][h-1] == '-') {
			around.push([v+1, h-1])
		}
	}

	// to bottom right
	if(v < box-1 && h < box-1) {
		if (position[v+1][h+1] == '-') {
			around.push([v+1, h+1])
		}
	}

	// console.log(around)

	return around
}

function follow_me(from, data, b_w, b_w_n) {
	var now = [from[0]+data[0], from[1]+data[1]]

	// cek show can cross border
	if (now[0] >= 0 || now[1] >= 0 || now[0] < now || now[1] < now) {
		if (!position[now[0]][now[1]] === undefined) { return false}
		
		var b_w_i = position[now[0]][now[1]]
		if (b_w_i == b_w) {
			return follow_me(now, data, b_w, b_w_n)
		} else if (b_w_i == b_w_n) {
			return true
		} else {
			return false
		}
	} else {
		return false
	}
}

function follow_with_data(from, data, b_w, b_w_n, arr = []) {
	var now = [from[0]-data[0], from[1]-data[1]]

		// console.log(now)
	// cek show can cross border
	if (now[0] < 0 || now[1] < 0 || now[0] >= now || now[1] >= now) {
		arr = []
		return arr
	}

	var b_w_i = position[now[0]][now[1]]
	if (b_w_i == b_w) {
		arr.push(now)
		return follow_with_data(now, data, b_w, b_w_n, arr)
	} else if (b_w_i == b_w_n) {
		return arr
	} else {
		arr = []
		return arr
	}
}