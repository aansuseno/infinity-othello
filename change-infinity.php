<?php 
require 'run.php';
require 'fun_koneksi.php';
require 'fun_legal.php';

$status = ($game['is_infinity'] == 1) ? 0 : 1;
change_infinity_status($status, $game['id']);

echo json_encode(['status' => $status]);
?>